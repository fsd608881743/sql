import { Component } from '@angular/core';

@Component({
  selector: 'app-regestration',
  templateUrl: './regestration.component.html',
  styleUrl: './regestration.component.css'
})
export class RegestrationComponent {
  constructor() {}

  registrationSubmit(formData: any) {
    // Implement your registration submission logic here
    console.log(formData); // For demonstration, you can log the form data
    alert('Registration successful'); // Display registration success message
  }

  empName: string = '';
  salary: number = 0;
  gender: string = '';
  doj: string = '';
  country: string = '';
  emailId: string = '';
  password: string = '';
  deptId: number = 0;
  mobileNumber: string = '';

  register() {
    // Regular expressions for validations
    const mobileRegex = /^[6-9]\d{9}$/; // Mobile number starting with 6, 7, 8, or 9 and exactly 10 digits
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/; // Basic email validation
    const passwordRegex = /^(?=.[a-z])(?=.[A-Z])(?=.\d)(?=.[@$!%?&^])[A-Za-z\d@$!%?&^]{8,}$/; // Password validation

    // Validation checks
    if (!this.empName || !this.salary || !this.gender || !this.doj || !this.country || !this.emailId || !this.password || !this.deptId || !this.mobileNumber) {
      alert('Please fill in all fields.');
      return; // Exit if any field is empty
    }

    if (!mobileRegex.test(this.mobileNumber)) {
      alert('Mobile number should be 10 digits and start with 6, 7, 8, or 9.');
      return; // Exit if mobile number is invalid
    }

    if (!emailRegex.test(this.emailId)) {
      alert('Invalid email address.');
      return; // Exit if email is invalid
    }

    if (!passwordRegex.test(this.password)) {
      alert('Password should be at least 8 characters long, contain at least one uppercase letter, one lowercase letter, one numeric digit, and one special symbol.');
      return; // Exit if password is invalid
    }

    // If all validations pass, proceed with registration
    this.registrationSubmit({
      empName: this.empName,
      salary: this.salary,
      gender: this.gender,
      doj: this.doj,
      country: this.country,
      emailId: this.emailId,
      password: this.password,
      deptId: this.deptId,
      mobileNumber: this.mobileNumber
    });
  }
}
