package com.dto;

public class Student {
	private int studentId;
	private String StudentName;
	private String Course;
	private String age;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}
	
	public Student(int studentId,String StudentName, String Course, String age, String gender,String emailId,String password) {
		super();
		 this.studentId=studentId;
		 this.StudentName=StudentName;
		 this. Course=Course;
		 this.age=age;
		 this.gender=gender;
		 this.emailId=emailId;
		 this.password=password;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public String getCourse() {
		return Course;
	}

	public void setCourse(String course) {
		Course = course;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "Employee [studentId=" + studentId + ", StudentName=" + StudentName + ", Course=" + Course + ", age=" + age
				+ ", gender=" + gender + ", emailId=" + emailId + ", password=" + password + "]";
	}

}
