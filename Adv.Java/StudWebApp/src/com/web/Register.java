package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/Register")
public class Register extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String StudentName = request.getParameter("StudentName");
		String Course = request.getParameter("Course");
		String age = request.getParameter("age");
		String gender = request.getParameter("gender");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		out.print("<html>");
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		out.print("<h1>Student Registration</h1>");
		out.print("<h3>StudentId   : " + studentId + "</h3>");
		out.print("<h3>Name : " + StudentName + "</h3>");
		out.print("<h3>course  : " + Course + "</h3>");
		out.print("<h3>Age  : " + age + "</h3>");
		out.print("<h3>Email: " + emailId + "</h3>");
		out.print("<h3>Password: " + password + "</h3>");
		out.print("<center></body></html>");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}