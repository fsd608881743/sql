SQL Day - 01
---------------
 
Models of Databaes (Network, Heirarchical, Relational(Tabular))
Relational:
The most common model, the relational model sorts data into tables, also known as relations, each of which consists of columns and rows.
Hierarchical model
The hierarchical model organizes data into a tree-like structure, where each record has a single parent or root. Sibling records are sorted in a particular order. That order is used as the physical order for storing the database. This model is good for describing many real-world relationships.
Network model
The network model builds on the hierarchical model by allowing many-to-many relationships between linked records, implying multiple parent records. Based on mathematical set theory, the model is constructed with sets of related records. Each set consists of one owner or parent record and one or more member or child records. A record can be a member or child in multiple sets, allowing this model to convey complex relationships.

Codd Rules for OLAP
1.	Multi-dimensional conceptual view of the database
2.	Concept of transparency
3.	Concept of accessibility
4.	Consistent reporting performance
5.	Client-server architecture
6.	Generic dimensionality
7.	Dynamic sparse matrix handling
8.	Multi-user support
9.	Unrestricted cross-dimensional operations
10.	Intuitive data manipulation
11.	Flexible reporting
12.	Unlimited dimensions and aggregation levels
RDBMS (Relational Database Management System)
RDBMS stands for Relational Database Management System.
RDBMS is a program used to maintain a relational database.
RDBMS is the basis for all modern database systems such as MySQL, Microsoft SQL Server, Oracle, and Microsoft Access.
RDBMS uses SQL queries to access the data in the database.
Database Server (Oracle, MySQL, MSSQLServer)
Database
  |
  V
Tables
  |
  V
Rows&Columns
  |
  V
 Data



Database Servers
----------------
Oracle
Sybase
MySql
MS SQL Server
DB2
Ingress
SQL Languages
1. DDL (Data Defination Language)     - Create, Alter, Drop
2. DML (Data Manipulate Language)     - Insert, Update, Delete
3. DQL (Data Query Language)             - Select
4. TCL (Transaction Control Language) - Commit, RollBack

Difference between char and varchar in mysql?
CHAR in MySQL stores characters of fixed length. VARCHAR in MySQL stores characters of variable size. CHAR in MySQL is used when the length of data is known so that we declare the field with the same length. VARCHAR in MySQL is used when the length of data is unknown.

