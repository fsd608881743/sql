SQL Day - 03
---------------
  
Auto Increment
*****************

create table student (id int primary key auto_increment, age int);
desc student;
+-------+------+------+-----+---------+----------------+
| Field | Type | Null | Key | Default | Extra          |
+-------+------+------+-----+---------+----------------+
| id    | int  | NO   | PRI | NULL    | auto_increment |
| age   | int  | YES  |     | NULL    |                |
+-------+------+------+-----+---------+----------------+
insert into student (age) values (22);
select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   22 |
+----+------+
insert into student (age) values (23);


mysql> insert into student (age) values (24);
 select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   22 |
|  2 |   23 |
|  3 |   24 |
+----+------+
 insert into student values(201,12);

select * from student;
+-----+------+
| id  | age  |
+-----+------+
|   1 |   22 |
|   2 |   23 |
|   3 |   24 |
| 201 |   12 |
+-----+------+

insert into student (age) values (25);

insert into student (age) values (33);

select * from student;
+-----+------+
| id  | age  |
+-----+------+
|   1 |   22 |
|   2 |   23 |
|   3 |   24 |
| 201 |   12 |
| 202 |   25 |
| 203 |   33 |
+-----+------+
drop table student;


setting auto value increment at the time of creation
****************************************************

mysql> create table student(id int primary key auto_increment, age int) auto_increment = 1001;

insert into student (age) values(22);


insert into student (age) values(25);

insert into student (age) values(21);

select * from student;
+------+------+
| id   | age  |
+------+------+
| 1001 |   22 |
| 1002 |   25 |
| 1003 |   21 |
+------+------+


insert into student values(2001,25);

insert into student (age) values(26);

insert into student (age) values(23);
select * from student;
+------+------+
| id   | age  |
+------+------+
| 1001 |   22 |
| 1002 |   25 |
| 1003 |   21 |
| 2001 |   25 |
| 2002 |   26 |
| 2003 |   23 |
+------+------+

Referential Integrity
*************************
1. On Delete Default
2. On Delete Cascade
3. On Delete Set Null


mysql> create database fsd60;

show databases;
+--------------------+
| Database           |
+--------------------+
| fsd60              |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+

use fsd60;
Database changed

1. On Delete Default
**************************

create table department(deptId int(5) primary key, deptName varchar(20), location varchar(20));


desc department;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| deptId   | int         | NO   | PRI | NULL    |       |
| deptName | varchar(20) | YES  |     | NULL    |       |
| location | varchar(20) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+

insert into department values (10,'IT','Hyd'),
    -> (20,'sales','chennai'),
    -> (30,'Testing','pune');

 select * from department;
+--------+----------+----------+
| deptId | deptName | location |
+--------+----------+----------+
|     10 | IT       | Hyd      |
|     20 | sales    | chennai  |
|     30 | Testing  | pune     |
+--------+----------+----------+

Create Employee Table Refe
mysql> create table employee (
    -> empId int(4) primary key,
    -> empName varchar(20),
    -> salary double(8, 2),
    -> gender varchar(6),
    -> emailId varchar(20) unique key,
    -> password varchar(20),
    -> deptId int,
    -> foreign key (deptId) references department(deptId));


mysql> insert into employee values (101, 'Harsha',  1212.12, 'Male',   'harsha@gmail.com',  '123', 10);

insert into employee values (102, 'Pasha',   1212.12, 'Male',   'pasha@gmail.com',   '123', 10);

insert into employee values (103, 'Indira',  1212.12, 'Female', 'indira@gmail.com',  '123', 20);

insert into employee values (104, 'Vamsi',   1212.12, 'Male', 'vamsi@gmail.com',   '123', 20);

insert into employee values (105, 'Deepika', 1212.12, 'Female', 'deepika@gmail.com', '123', 30);

insert into employee values (106, 'Utkarsh', 1212.12, 'Male',   'utkarsh@gmail.com', '123', 30);

Select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |     10 |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |     10 |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |     30 |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |     30 |
+-------+---------+---------+--------+-------------------+----------+--------+


 select * from department;
+--------+----------+----------+
| deptId | deptName | location |
+--------+----------+----------+
|     10 | IT       | Hyd      |
|     20 | Sales    | Chennai  |
|     30 | Testing  | Pune     |
+--------+----------+----------+

select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |     10 |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |     10 |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |     30 |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |     30 |
+-------+---------+---------+--------+-------------------+----------+--------+

delete from department where deptId = 10;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails (`fsd600`.`employee`, CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`deptId`) REFERENCES `department` (`deptId`))

delete from employee where empId = 101;

delete from employee where empId = 102;

delete from employee where empId =103;

 delete from department where deptId =20;
ERROR 1451 (23000): Cannot delete or update a parent row: a foreign key constraint fails (`fsd600`.`employee`, CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`deptId`) REFERENCES `department` (`deptId`))


drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.

drop table employee;
Query OK, 0 rows affected (0.04 sec)

drop table department;
Query OK, 0 rows affected (0.01 sec)

2. on delete cascade
***********************
create table department (
deptId int(4) primary key, 
deptName varchar(20),
location varchar(20));


 insert into department values
    -> (10, 'IT', 'Hyd'),
    -> (20, 'Sales', 'Chennai'),
    -> (30, 'Testing', 'Pune')
    -> ,(40,'Operations','Hyderabad');
select * from department;
+--------+------------+-----------+
| deptId | deptName   | location  |
+--------+------------+-----------+
|     10 | IT         | Hyd       |
|     20 | Sales      | Chennai   |
|     30 | Testing    | Pune      |
|     40 | Operations | Hyderabad |
+--------+------------+-----------+


mysql> create table employee (
    -> empId int(4) primary key,
    -> empName varchar(20),
    -> salary double(8, 2),
    -> gender varchar(6),
    -> emailId varchar(20) unique key,
    -> password varchar(20),
    -> deptId int,
    -> foreign key (deptId) references department(deptId) on delete cascade);

insert into employee values (101, 'Harsha',  1212.12, 'Male',   'harsha@gmail.com',  '123', 10);

insert into employee values (102, 'Pasha',   1212.12, 'Male',   'pasha@gmail.com',   '123', 10);

insert into employee values (103, 'Indira',  1212.12, 'Female', 'indira@gmail.com',  '123', 20);

insert into employee values (104, 'Vamsi',   1212.12, 'Male',   'vamsi@gmail.com',   '123', 20);

insert into employee values (105, 'Deepika', 1212.12, 'Female', 'deepika@gmail.com', '123', 30);

insert into employee values (106, 'Utkarsh', 1212.12, 'Male',   'utkarsh@gmail.com', '123', 30);

select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |     10 |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |     10 |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |     30 |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |     30 |
+-------+---------+---------+--------+-------------------+----------+--------+

select * from department;
+--------+------------+-----------+
| deptId | deptName   | location  |
+--------+------------+-----------+
|     10 | IT         | Hyd       |
|     20 | Sales      | Chennai   |
|     30 | Testing    | Pune      |
|     40 | Operations | Hyderabad |
+--------+------------+-----------+

delete from department where deptId=30;

select * from department;
+--------+------------+-----------+
| deptId | deptName   | location  |
+--------+------------+-----------+
|     10 | IT         | Hyd       |
|     20 | Sales      | Chennai   |
|     40 | Operations | Hyderabad |
+--------+------------+-----------+

select * from employee;
+-------+---------+---------+--------+------------------+----------+--------+
| empId | empName | salary  | gender | emailId          | password | deptId |
+-------+---------+---------+--------+------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com | 123      |     10 |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com  | 123      |     10 |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com  | 123      |     20 |
+-------+---------+---------+--------+------------------+----------+--------+

drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.
mysql> drop table employee;
drop table employee;

drop table department;


3. on delete set null
**************************

create table department (
    -> deptId int(4) primary key,
    -> deptName varchar(20),
    -> location varchar(20));

insert into department values
    -> (10, 'IT', 'Hyd'),
    -> (20, 'Sales', 'Chennai'),
    -> (30, 'Testing', 'Pune');
    -> (40,'operations','Delhi');

select * from department;
+--------+------------+----------+
| deptId | deptName   | location |
+--------+------------+----------+
|     10 | IT         | Hyd      |
|     20 | Sales      | Chennai  |
|     30 | Testing    | Pune     |
|     40 | operations | Delhi    |
+--------+------------+----------+


select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |     10 |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |     10 |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |   NULL |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |   NULL |
+-------+---------+---------+--------+-------------------+----------+--------+

select * from department;
+--------+------------+----------+
| deptId | deptName   | location |
+--------+------------+----------+
|     10 | IT         | Hyd      |
|     20 | Sales      | Chennai  |
|     40 | operations | Delhi    |
+--------+------------+----------+

delete from department where deptId=10;

select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |   NULL |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |   NULL |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |   NULL |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |   NULL |
+-------+---------+---------+--------+-------------------+----------+--------+

select * from employee;
+-------+---------+---------+--------+-------------------+----------+--------+
| empId | empName | salary  | gender | emailId           | password | deptId |
+-------+---------+---------+--------+-------------------+----------+--------+
|   101 | Harsha  | 1212.12 | Male   | harsha@gmail.com  | 123      |   NULL |
|   102 | Pasha   | 1212.12 | Male   | pasha@gmail.com   | 123      |   NULL |
|   103 | Indira  | 1212.12 | Female | indira@gmail.com  | 123      |     20 |
|   104 | Vamsi   | 1212.12 | Male   | vamsi@gmail.com   | 123      |     20 |
|   105 | Deepika | 1212.12 | Female | deepika@gmail.com | 123      |   NULL |
|   106 | Utkarsh | 1212.12 | Male   | utkarsh@gmail.com | 123      |   NULL |
+-------+---------+---------+--------+-------------------+----------+--------+

 drop table department;
ERROR 3730 (HY000): Cannot drop table 'department' referenced by a foreign key constraint 'employee_ibfk_1' on table 'employee'.

drop table employee;

 
drop table department;
