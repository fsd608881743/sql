SQL - Day 02
--------------------
  

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+

mysql> create database fsd60;

mysql> use fsd60;
Database changed
mysql> create table student(id int(10), fullname varchar(20), age int, course varchar(10), fees double(8,2));

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| fsd60              |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+

mysql> show tables;
+-----------------+
| Tables_in_fsd60 |
+-----------------+
| student         |
+-----------------+

mysql> desc student;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| id       | int         | YES  |     | NULL    |       |
| fullname | varchar(20) | YES  |     | NULL    |       |
| age      | int         | YES  |     | NULL    |       |
| course   | varchar(10) | YES  |     | NULL    |       |
| fees     | double(8,2) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+


insert into student values (1, 'Afrid', 23, 'java', 3000.00);
insert into student values (2, 'samar', 23, 'python',5000.00);
insert into student values (3, 'pavan',22, 'java',4000.3325);
insert into student values (4,'manoj',24,'javascript',5500.55);
insert into student values (5, 'ashoke',22,'MERN',6001.333);




mysql> select * from student;
+------+----------+------+------------+---------+
| id   | fullname | age  | course     | fees    |
+------+----------+------+------------+---------+
|    1 | Afrid    |   23 | java       | 3000.00 |
|    2 | samar    |   23 | python     | 5000.00 |
|    3 | pavan    |   22 | java       | 4000.33 |
|    4 | manoj    |   24 | javascript | 5500.55 |
|    5 | ashoke   |   22 | MERN       | 6001.33 |
+------+----------+------+------------+---------+


alter table student add column mobile varchar(15);


mysql> select * from student;
+------+----------+------+------------+---------+--------+
| id   | fullname | age  | course     | fees    | mobile |
+------+----------+------+------------+---------+--------+
|    1 | Afrid    |   23 | java       | 3000.00 | NULL   |
|    2 | samar    |   23 | python     | 5000.00 | NULL   |
|    3 | pavan    |   22 | java       | 4000.33 | NULL   |
|    4 | manoj    |   24 | javascript | 5500.55 | NULL   |
|    5 | ashoke   |   22 | MERN       | 6001.33 | NULL   |
+------+----------+------+------------+---------+--------+

alter table student drop column mobile;

select * from student;
+------+----------+------+------------+---------+
| id   | fullname | age  | course     | fees    |
+------+----------+------+------------+---------+
|    1 | Afrid    |   23 | java       | 3000.00 |
|    2 | samar    |   23 | python     | 5000.00 |
|    3 | pavan    |   22 | java       | 4000.33 |
|    4 | manoj    |   24 | javascript | 5500.55 |
|    5 | ashoke   |   22 | MERN       | 6001.33 |
+------+----------+------+------------+---------+


delete from student;

show tables;
+-----------------+
| Tables_in_fsd60 |
+-----------------+
| student         |
+-----------------+

select * from student;
Empty set (0.00 sec)

mysql> drop table student;
show tables;
Empty set (0.00 sec)

show databases;
+--------------------+
| Database           |
+--------------------+
| fsd60              |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+

drop database fsd60;

show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+


mysql> show databases ;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+
create database fsd60;

use fsd60;
Database changed
create table student (id int(5), name varchar(20), age int, course varchar(10),fees double(8,2));

insert into student values (1,'Afrid',22,'java',2500.00);

insert into student values(2,'pavan',23,'java',3000.00);

insert into student values(3,'manoj',25,'java',5000.00);

show tables;
+-----------------+
| Tables_in_fsd60 |
+-----------------+
| student         |
+-----------------+

select * from student;
+------+-------+------+--------+---------+
| id   | name  | age  | course | fees    |
+------+-------+------+--------+---------+
|    1 | Afrid |   22 | java   | 2500.00 |
|    2 | pavan |   23 | java   | 3000.00 |
|    3 | manoj |   25 | java   | 5000.00 |
+------+-------+------+--------+---------+

insert into student (id,name,age,course,fees) values (4,'ashoke',25,'python',2255.00);

insert into student (name,id,age,fees,course) values ('saqeebh',5,25,6000.00,'HTML');

select * from student;
+------+---------+------+--------+---------+
| id   | name    | age  | course | fees    |
+------+---------+------+--------+---------+
|    1 | Afrid   |   22 | java   | 2500.00 |
|    2 | pavan   |   23 | java   | 3000.00 |
|    3 | manoj   |   25 | java   | 5000.00 |
|    4 | ashoke  |   25 | python | 2255.00 |
|    5 | saqeebh |   25 | HTML   | 6000.00 |
+------+---------+------+--------+---------+

insert into student values (1,'sahil',25,'sql',25232.1);

select * from student;
+------+---------+------+--------+----------+
| id   | name    | age  | course | fees     |
+------+---------+------+--------+----------+
|    1 | Afrid   |   22 | java   |  2500.00 |
|    2 | pavan   |   23 | java   |  3000.00 |
|    3 | manoj   |   25 | java   |  5000.00 |
|    4 | ashoke  |   25 | python |  2255.00 |
|    5 | saqeebh |   25 | HTML   |  6000.00 |
|    1 | sahil   |   25 | sql    | 25232.10 |
+------+---------+------+--------+----------+


mysql> insert into student values (1,'sahil',25,'sql',25232.1);

select * from student;
+------+---------+------+--------+----------+
| id   | name    | age  | course | fees     |
+------+---------+------+--------+----------+
|    1 | Afrid   |   22 | java   |  2500.00 |
|    2 | pavan   |   23 | java   |  3000.00 |
|    3 | manoj   |   25 | java   |  5000.00 |
|    4 | ashoke  |   25 | python |  2255.00 |
|    5 | saqeebh |   25 | HTML   |  6000.00 |
|    1 | sahil   |   25 | sql    | 25232.10 |
+------+---------+------+--------+----------+


insert into student values(7,'sai',32,'c++',2600.33),
    -> (8,'aleem',23,'java',262.33),
    -> (9,'sharma',25,'html',23235.33);

select * from student;
+------+---------+------+--------+----------+
| id   | name    | age  | course | fees     |
+------+---------+------+--------+----------+
|    1 | Afrid   |   22 | java   |  2500.00 |
|    2 | pavan   |   23 | java   |  3000.00 |
|    3 | manoj   |   25 | java   |  5000.00 |
|    4 | ashoke  |   25 | python |  2255.00 |
|    5 | saqeebh |   25 | HTML   |  6000.00 |
|    1 | sahil   |   25 | sql    | 25232.10 |
|    7 | sai     |   32 | c++    |  2600.33 |
|    8 | aleem   |   23 | java   |   262.33 |
|    9 | sharma  |   25 | html   | 23235.33 |
+------+---------+------+--------+----------+

update student set id=6 where name='sahil';

select * from student;
+------+---------+------+--------+----------+
| id   | name    | age  | course | fees     |
+------+---------+------+--------+----------+
|    1 | Afrid   |   22 | java   |  2500.00 |
|    2 | pavan   |   23 | java   |  3000.00 |
|    3 | manoj   |   25 | java   |  5000.00 |
|    4 | ashoke  |   25 | python |  2255.00 |
|    5 | saqeebh |   25 | HTML   |  6000.00 |
|    6 | sahil   |   25 | sql    | 25232.10 |
|    7 | sai     |   32 | c++    |  2600.33 |
|    8 | aleem   |   23 | java   |   262.33 |
|    9 | sharma  |   25 | html   | 23235.33 |
+------+---------+------+--------+----------+

update student set fees=0 where course ='python';
Rselect * from student;
+------+---------+------+--------+----------+
| id   | name    | age  | course | fees     |
+------+---------+------+--------+----------+
|    1 | Afrid   |   22 | java   |  2500.00 |
|    2 | pavan   |   23 | java   |  3000.00 |
|    3 | manoj   |   25 | java   |  5000.00 |
|    4 | ashoke  |   25 | python |     0.00 |
|    5 | saqeebh |   25 | HTML   |  6000.00 |
|    6 | sahil   |   25 | sql    | 25232.10 |
|    7 | sai     |   32 | c++    |  2600.33 |
|    8 | aleem   |   23 | java   |   262.33 |
|    9 | sharma  |   25 | html   | 23235.33 |
+------+---------+------+--------+----------+
select name from student;
+---------+
| name    |
+---------+
| Afrid   |
| pavan   |
| manoj   |
| ashoke  |
| saqeebh |
| sahil   |
| sai     |
| aleem   |
| sharma  |
+---------+

select age from student where age<25;
+------+
| age  |
+------+
|   22 |
|   23 |
|   23 |
+------+
select course from studetn where course='python;
 select course from student where course='python';
+--------+
| course |
+--------+
| python |
+--------+ 

Constraints in SQL
------------------
1. Not Null

create database fsd60;

show databases;
+--------------------+
| Database           |
+--------------------+
| fsd60              |
| information_schema |
| mysql              |
| performance_schema |
| sakila             |
| sys                |
| world              |
+--------------------+

use fsd60;
Database changed
create table student(id int not null, age int);

desc student;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | NO   |     | NULL    |       |
| age   | int  | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+

insert into student values(1,null);

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 | NULL |
+----+------+

2. Unique
---------

show tables;

create table student (id int unique key , age int);

desc student ;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | YES  | UNI | NULL    |       |
| age   | int  | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+

insert into student values (1,50);

insert into student values(1,20);
ERROR 1062 (23000): Duplicate entry '1' for key 'student.id'
insert into student values(2,50);

mysql> select * from student;
+------+------+
| id   | age  |
+------+------+
|    1 |   50 |
|    2 |   50 |
+------+------+


Unique and Not Null
-------------------

show tables;

create table student (id int unique not null,age int);

desc student;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | NO   | PRI | NULL    |       |
| age   | int  | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+

insert into student values(1,50);

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   50 |
+----+------+

insert into student vlues(null,null);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'vlues(null,null)' at line 1

insert into student values(null,20);
ERROR 1048 (23000): Column 'id' cannot be null

insert into student values (2,null);

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   50 |
|  2 | NULL |
+----+------+

insert into student values(2,30);
ERROR 1062 (23000): Duplicate entry '2' for key 'student.id'
insert into student values(3,null);
OK, 1 row affected (0.01 sec)

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   50 |
|  2 | NULL |
|  3 | NULL |
+----+------+


3. Primary Key (Combination of Unique + Not Null)
--------------

create table student (id int primary key, age int);

desc student;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | NO   | PRI | NULL    |       |
| age   | int  | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+

insert into student values(1,20);

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   20 |
+----+------+

mysql> insert into student values(null,20);
ERROR 1048 (23000): Column 'id' cannot be null
mysql> insert into student values(1,20);
ERROR 1062 (23000): Duplicate entry '1' for key 'student.PRIMARY'
mysql> insert into student values(2,20);
Query OK, 1 row affected (0.01 sec)

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   20 |
|  2 |   20 |
+----+------+

insert into student values(3,null);

select * from student;
+----+------+
| id | age  |
+----+------+
|  1 |   20 |
|  2 |   20 |
|  3 | NULL |
+----+------+


4. Check
--------
create table student (id int, age int check(age>12 and age<20));

desc student;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | YES  |     | NULL    |       |
| age   | int  | YES  |     | NULL    |       |
+-------+------+------+-----+---------+-------+

insert into student values(1,13);

select * from student;
+------+------+
| id   | age  |
+------+------+
|    1 |   13 |
+------+------+

mysql> insert into student values(2,6);
ERROR 3819 (HY000): Check constraint 'student_chk_1' is violated.
mysql> insert into student values(2,20);
ERROR 3819 (HY000): Check constraint 'student_chk_1' is violated.
mysql> insert into student values(2,15);
Query OK, 1 row affected (0.01 sec)

select * from student;
+------+------+
| id   | age  |
+------+------+
|    1 |   13 |
|    2 |   15 |
+------+------+


5. Default
----------
create table student (id int, age int default 20);


mdesc student;
+-------+------+------+-----+---------+-------+
| Field | Type | Null | Key | Default | Extra |
+-------+------+------+-----+---------+-------+
| id    | int  | YES  |     | NULL    |       |
| age   | int  | YES  |     | 20      |       |
+-------+------+------+-----+---------+-------+

insert into student values(101,25);

mysql> inseret into student values(102);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'inseret into student values(102)' at line 1
mysql> insert into student values(102);
ERROR 1136 (21S01): Column count doesn't match value count at row 1
insert into student values(102,26);

insert into student (id) values (103);
select * from student;
+------+------+
| id   | age  |
+------+------+
|  101 |   25 |
|  102 |   26 |
|  103 |   20 |
+------+------+

